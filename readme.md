
### Negative Egg SixtyFour

A toy to practice using websockets and publishing different views to different pages.

// Run by starting server, then navigating one tab of a web browser to sink.html and one to listener.html

Required libraries : //[Java-WebSocket](https://search.maven.org/#artifactdetails%7Corg.java-websocket%7CJava-WebSocket%7C1.3.0%7Cjar) and [gson](http://search.maven.org/#artifactdetails%7Ccom.google.code.gson%7Cgson%7C2.8.0%7Cjar)

#### Run

Interaction involves putting text in the sink's box, and pressing the button. (This will make the text appear in the sink and uppercased in the listener.) You can also revert to the previous entry by pressing undo on the listener.

#### Reason

The project I intend will be simpler, initially, if I can make several pages, rather than a single morphing page. This is a doodle to practice doing so.

'Negative Rain SixtyFour' is just a three word phrase chosen to avoid titling this project 'websocket demo #25395'. It has no other overt significance.

