
/*
*/

var termin = function() {};
termin.log = function( message )
{
	try
	{ console.log( message ); }
	catch ( exception )
	{ return; } // IE reputedly has no console.
}


const messageVersionIdentity = 1;
const messageVersionNewVal = 1;
var channel;


var supported = "WebSocket" in window;
if ( ! supported )
{
	document.getElementById( "btnSubmit" ).disabled = true; 
}
else
{
	channel = new WebSocket( "ws://localhost:9998" );
	channel.onopen = function coo()
	{
		termin.log( "connected" );
		channel.send( JSON.stringify( selfIdentify() ) );
	};
	channel.onclose = function coc()
	{
		termin.log( "glad that's over" );
	};
	channel.onmessage = function com( msg )
	{
		applyResponse( msg.data );
	};
}


function selfIdentify()
{
	return { "msgType" : "identity", "value" : "ne64:sink",
		"version" : messageVersionIdentity };
}


function sendInput()
{
	var tfName = document.getElementById( "tfName" ).value;
	return channel.send( JSON.stringify( { "msgType" : "newVal", "value" : tfName,
		"version" : messageVersionNewVal } ) );
}


//show the new value
function applyResponse( stillJson )
{
	var response = JSON.parse( stillJson );
	if ( response.msgType == "newVal"
			&& response.version > 0 )
	{
		document.getElementById( "tfOut" ).innerText = response.value;
	}
	else if ( response.msgType == "identify"
		&& response.version > 0 )
	{
		channel.send( JSON.stringify( selfIdentify() ) );
		alert( "Please resend the data" );
	}
	else
	{
		termin.log( response.msgType +" is not a response I handle" );
		// perhaps other failure states
	}
}



























