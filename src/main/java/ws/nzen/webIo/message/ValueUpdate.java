package ws.nzen.webIo.message;

public class ValueUpdate extends TypeMessage
{
	private String value;

	public String getValue()
	{
		return value;
	}

	public ValueUpdate setValue( String value )
	{
		this.value = value;
		return this;
	}

}
