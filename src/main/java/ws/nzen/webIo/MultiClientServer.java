package ws.nzen.webIo;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import com.google.gson.Gson;

import ws.nzen.webIo.message.HasType;
import ws.nzen.webIo.message.TypeMessage;
import ws.nzen.webIo.message.ValueUpdate;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**  */
public class MultiClientServer extends WebSocketServer
{
	private Gson jsParser = new Gson();
	private String previousModel = "";
	private String model = "initial value";
	private int mcsServerVersion = 1;
	private Map<Integer, String> sessionMetadata = new HashMap<>();
	private static final String SINK_CONNECTION = "sink";
	private static final String CLIENT_PREFIX = "ne64:";


	public static void main( String[] args ) throws UnknownHostException
	{
		MultiClientServer conch = new MultiClientServer();
		conch.start();
		System.out.println( "Server ready for clients. Restart them if they're open." );
	}


	public MultiClientServer() throws UnknownHostException
	{
		super( new InetSocketAddress( 9998 ) );
	}


	@Override
	public void onOpen( WebSocket conn, ClientHandshake handshake )
	{
		System.out.println( "began with "+ conn.getLocalSocketAddress() + conn.hashCode() );
	}


	@Override
	public void onMessage( WebSocket conn, String message )
	{
		System.out.println( "received "+ message +" "+ conn.hashCode() );
		String reply = replyToMessage( conn.hashCode(), message );
		if ( ! ( reply == null || reply.isEmpty() ) )
		{
			conn.send( reply );
		}
	}


	@Override
	public void onClose( WebSocket conn, int code, String reason, boolean remote )
	{
		System.out.println( "finished with "+ conn.getLocalSocketAddress() );
		int sessionId = conn.hashCode();
		if ( sessionMetadata.containsKey( sessionId ) )
		{
			sessionMetadata.remove( sessionId );
		}
	}


	@Override
	public void onError( WebSocket conn, Exception ex )
	{
		System.err.println( "failed with "+ conn.getLocalSocketAddress() +"\n"+ ex );
	}


	private String replyToMessage( int sessionId, String json )
	{
		TypeMessage firstPass = jsParser.fromJson( json, TypeMessage.class );
		if ( ! ( firstPass.getMsgType().equals( TypeMessage.TYPE_IDENTITY )
				|| sessionMetadata.containsKey( sessionId ) ) )
		{
			// if I don't know you, I'm ignoring you
			return requestIdentity();
		}
		switch ( firstPass.getMsgType() )
		{
			case TypeMessage.TYPE_IDENTITY :
			{
				ValueUpdate hasNewVal = jsParser.fromJson( json, ValueUpdate.class );
				return acknowledgeIdentity( sessionId, hasNewVal.getValue() );
			}
			case TypeMessage.TYPE_NEW_VALUE :
			{
				ValueUpdate hasNewVal = jsParser.fromJson( json, ValueUpdate.class );
				return propagateNewModel( sessionId, hasNewVal.getValue() );
			}
			case TypeMessage.TYPE_UNDO :
			{
				return undoLastChange( sessionId );
			}
			default :
			{
				return "";
			}
		}
	}


	/** unknown connection made a request; ignore and demand identity */
	private String requestIdentity()
	{
		TypeMessage sayYourName = new TypeMessage();
		sayYourName.setMsgType( "identify" );
		sayYourName.setVersion( 1 );
		return jsParser.toJson( sayYourName, TypeMessage.class );
	}


	private String acknowledgeIdentity( int sessionId, String reportedType )
	{
		if ( reportedType.startsWith( CLIENT_PREFIX ) )
		{
			sessionMetadata.put( sessionId, reportedType
					.substring( CLIENT_PREFIX.length() ) );
		}
		return ""; // not really acknowledging }:)
	}


	private String undoLastChange( int initiatorId )
	{
		return propagateNewModel( initiatorId, previousModel );
	}


	private String propagateNewModel( int initiatorId, String newVal )
	{
		// handle the update
		previousModel = model;
		model = newVal;
		ValueUpdate hasNewVal = new ValueUpdate();
		hasNewVal.setValue( model ).setMsgType( TypeMessage.TYPE_NEW_VALUE ).setVersion( 1 );
		String replyForSink = jsParser.toJson( hasNewVal );
		hasNewVal.setValue( hasNewVal.getValue().toUpperCase() );
		String replyForListener = jsParser.toJson( hasNewVal );
		// warn everyone except initiator
		int currId;
		for ( WebSocket currConnection : connections() )
		{
			currId = currConnection.hashCode();
			if ( currId != initiatorId )
			{
				if ( sessionMetadata.get( currId ).equals( SINK_CONNECTION ) )
				{
					currConnection.send( replyForSink );
				}
				else
				{
					currConnection.send( replyForListener );
				}
			}
		}
		if ( sessionMetadata.get( initiatorId ).equals( SINK_CONNECTION ) )
		{
			return replyForSink;
		}
		else
		{
			return replyForListener;
		}
	}


}








































